# Configurar *Node-RED*
Autor: Jhonyfer Angarita Moreno


## Agregar un robot/nodo a Node-RED-PRIA
1. Crear un robot en la carpeta *nodes* en *src*, con archivos de tipo .js y .html. Se puede usar como ejemplo el nodo *maya*. Definir:
    - Propiedades: Toda propiedad que deba cargarse a FireStore debe terminar en *Prop*. Incluir *typeProp*.
    - Campos de entrada: El html debe tener selectores o campos de entrada para modificar las propiedades del nodo.
    - Scripts en el archivo html que modifican las propiedades al interactuar con la interfaz web.
2. Añadir el nuevo robot/nodo al archivo *package.json*, en el bloque *node-red*, agregando su ruta. Usar como ejemplo los nodos ya agregados.
3. Compilar los archivos *TypeScript*, copiar archivos *html* y reiniciar node-RED, usando los siguientes comandos:
    ```bash
    tsc && npm run visual && pm2 restart node-red
    ```
4. Verifcar en Node-RED que el nodo se encuentra en el pallet. Si falla, revisar el código. Se recomienda borrar el nodo de la interfaz web despues de hacer cambios en el código de dicho nodo y hacer deploy sin el nodo para guardar los cambios en el servidor.


## Hacer que el nodo pueda ser leido por *linker.ts*
Añadir el nombre del nodo al archivo *linker.ts*, en el arreglo *validTypes*


## Verificar que el nodo genera un documento en FireStore
1. Añadir el nodo que se está desarrollando a la interfaz web de *Node-RED* en un flujo activo
2. Hacer *deploy* para guardar los cambios en el servidor.
3. Ejecutar el *linker.js* y verificar que no existan errores. Añadir como argumento el número del flow. Ejecutar los siguientes comandos en el servidor:
    ```bash
    cd Documents/node-red-pria
    node dist/utils/linker.js 1
    ```
4. Revisar firestore y comprobar que se cargan los argumentos deseados. (Consultar con el desarrollador si se pueden cargar argumentos distintos a números)


## Agregar flows al script *linker.ts*
El script *linker.ts* tiene *harcodeadeos* los ids de los *flows* que puede leer. Es necesario editar este archivo si se desea añadir un nuevo *flow*
1. Revisar el archivo *flows_{host}.json*. Buscar en el archivo las claves *type* y revisar si el valor es *tab* (indica que el arreglo describe un *flow*). En ese mismo arreglo, buscar la clave *label*. Si *label* coincide con el nombre del *flow* que se desea agregar, ir a la clave *id* y copiar el valor de esa clave.
2. Ir al archivo *linker.ts*, dentro de la carpeta *src/utils* del paquete *node-red-pria*. En el arreglo *flows* añadir o editar el *id* del *flow* deseado
3. Recompilar usando el comando *tsc*
4. Verificar que el *flow* añadido puede ser cargado a *Firestore*. Ejemplo para *Flow 1*:
    ```bash
    cd Documents/node-red-pria
    node dist/utils/linker.js 1
    ```

## Asegurar Node-RED con contraseña
1. Revisar la página https://nodered.org/docs/user-guide/runtime/securing-node-red#usernamepassword-based-authentication para añadir uno o varios usuarios y contraseñas a Node-RED
2. Reiniciar Node-RED y verificar que se pide contraseña al intentar ingresar.
3. Para que el script *linker.ts* puede hacer algún request a *Node-RED*, debe obtenerse un token, asociado a un usuario. Visitar la siguiente página para saber como se obtienen los tokens: https://nodered.org/docs/api/admin/oauth
4. Añadir el token al archivo *linker.ts*, en la constante *access_token*. Puede verificarse el tiempo de expiración del token revisando el archivo *.seesions.json* dentro de la carpeta *.node-red*.
5. Compilar el paquete *node-red-pria* con el comando tsc
6. Verificar que se crea un proceso con el siguiente comando, (el *flow* debe ser accesible desde *linker.ts*, ver *Agregar flows al script linker.ts*):
    ```bash
    cd Documents/node-red-pria
    node dist/utils/linker.js 1
    ```

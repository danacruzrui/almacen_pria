import { Red, Node } from "node-red";

module.exports = function(RED: Red) {
  function almacenNode(this: any, config: any) {
    RED.nodes.createNode(this, config);
    var node: Node = this;
    node.on("input", async (msg: any) => {
      console.log("config", config);
    });
  }
  RED.nodes.registerType("ALMACEN", almacenNode);
}
